import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ResultSetsPeople } from '../model/result-sets-people';
import { ResultSetsPlanets } from '../model/result-sets-planets';
import { ResultSetsFilms } from '../model/result-sets-films';
import { ResultSetsSpecies } from '../model/result-sets-species';
import { ResultSetsVehicles } from '../model/result-sets-vehicles';
import { ResultSetsStarships } from '../model/result-sets-starships';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  constructor(private httpClient: HttpClient) { }

  getPeople(): Observable<ResultSetsPeople>{
    return this.httpClient.get<ResultSetsPeople>(environment.serverRestAPI + '/people/');
  }

  getPlanets(): Observable<ResultSetsPlanets>{
    return this.httpClient.get<ResultSetsPlanets>(environment.serverRestAPI + '/planets/');
  }

  getFilms(): Observable<ResultSetsFilms>{
    return this.httpClient.get<ResultSetsFilms>(environment.serverRestAPI + '/films/');
  }

  getSpecies(): Observable<ResultSetsSpecies>{
    return this.httpClient.get<ResultSetsSpecies>(environment.serverRestAPI + '/species/');
  }

  getVehicles(): Observable<ResultSetsVehicles>{
    return this.httpClient.get<ResultSetsVehicles>(environment.serverRestAPI + '/vehicles/');
  }

  getStarships(): Observable<ResultSetsStarships>{
    return this.httpClient.get<ResultSetsStarships>(environment.serverRestAPI + '/starships/');
  }
}
