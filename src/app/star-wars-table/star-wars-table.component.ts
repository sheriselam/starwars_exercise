import { Component, OnInit, Input } from '@angular/core';
import { DataServiceService } from '../services/data-service.service';
import { People } from '../model/people';
import { Planets } from '../model/planets';
import { Films } from '../model/films';
import { Species } from '../model/species';
import { Vehicles } from '../model/vehicles';
import { Starships } from '../model/starships';

@Component({
  selector: 'app-star-wars-table',
  templateUrl: './star-wars-table.component.html',
  styleUrls: ['./star-wars-table.component.css']
})
export class StarWarsTableComponent implements OnInit {

  @Input() tableName: string;

  people: People[];
  planets: Planets[];
  films: Films[];
  species: Species[];
  vehicles: Vehicles[];
  starShips: Starships[];

  constructor(private dataSS: DataServiceService) {}

 showTable(event){
   this.tableName = event.target.value;
    if(this.tableName === 'People'){
      this.dataSS.getPeople().subscribe(
        data => {
          console.log(data);
          this.people = data.results;
        },
        error => {
          console.log('An error occured');
          console.log(error);
        }
      );
    }

    if(this.tableName === 'Planets'){
      this.dataSS.getPlanets().subscribe(
        data => {
          console.log(data);
          this.planets = data.results;
        },
        error => {
          console.log('An error occured');
          console.log(error);
        }
      );
    }

    if(this.tableName === 'Films'){
      this.dataSS.getFilms().subscribe(
        data => {
          console.log(data);
          this.films = data.results;
        },
        error => {
          console.log('An error occured');
          console.log(error);
        }
      );
    }

    if(this.tableName === 'Species'){
      this.dataSS.getSpecies().subscribe(
        data => {
          console.log(data);
          this.species = data.results;
        },
        error => {
          console.log('An error occured');
          console.log(error);
        }
      );
    }

    if(this.tableName === 'Vehicles'){
      this.dataSS.getVehicles().subscribe(
        data => {
          console.log(data);
          this.vehicles = data.results;
        },
        error => {
          console.log('An error occured');
          console.log(error);
        }
      );
    }

    if(this.tableName === 'Starships'){
      this.dataSS.getStarships().subscribe(
        data => {
          console.log(data);
          this.starShips = data.results;
        },
        error => {
          console.log('An error occured');
          console.log(error);
        }
      );
    }
 }

  ngOnInit() {
   // console.log('Data being passed: ' + this.tableName);
  }

}