import { People } from './people';

export class ResultSetsPeople {
    constructor(public count: number,
        public next: string,
        public previous: string,
        public results: People[]){}
}
