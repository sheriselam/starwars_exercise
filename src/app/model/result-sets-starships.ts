import { Starships } from './starships';

export class ResultSetsStarships {
    constructor(public count: number,
        public next: string,
        public previous: string,
        public results: Starships[]){}
}
