import { Vehicles } from './vehicles';

export class ResultSetsVehicles {
    constructor(public count: number,
        public next: string,
        public previous: string,
        public results: Vehicles[]){}
}
