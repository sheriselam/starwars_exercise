export class Starships {
    constructor(public name: string, public model: string,
        starship_class: string,
        cost_in_credits: number,
        length: number,
        max_atmosphering_speed: number,
        crew: number,
        passengers: number,
        pilots: string){}
}
