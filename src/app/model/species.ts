export class Species {
    constructor(public name: string, public classification: string,
        designation: string,
        average_height: number,
        skin_colors: string,
        hair_colors: string,
        eye_colors: string,
        average_lifespan: number,
        language: string){}
}
