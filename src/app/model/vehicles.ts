export class Vehicles {
    constructor(public name: string, public model: string,
        vehicle_class: string,
        cost_in_credits: number,
        length: number,
        max_atmosphering_speed: number,
        crew: number,
        passengers: number,
        pilots: string
        ) {}
}
