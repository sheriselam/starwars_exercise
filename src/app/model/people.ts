export class People {
    constructor(public name: string, public height: number,
        mass: number,
        hair_color: string,
        skin_color: string,
        eye_color: string,
        birth_year: string,
        gender: string) {}
}
