import { Planets } from './planets';

export class ResultSetsPlanets {
    constructor(public count: number,
        public next: string,
        public previous: string,
        public results: Planets[]){}
}
