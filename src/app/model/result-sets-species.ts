import { Species } from './species';

export class ResultSetsSpecies {
    constructor(public count: number,
        public next: string,
        public previous: string,
        public results: Species[]){}
}
