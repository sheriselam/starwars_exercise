import { Films } from './films';

export class ResultSetsFilms {

    constructor(public count: number,
        public next: string,
        public previous: string,
        public results: Films[]){}
}
