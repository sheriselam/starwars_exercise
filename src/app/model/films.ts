export class Films {
    constructor(public title: string, public episode_id: number,
        opening_crawl: string,
        director: string,
        producer: string,
        release_date: string){}
}
