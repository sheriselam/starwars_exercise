import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StarWarsTableComponent } from './star-wars-table/star-wars-table.component';

const routes: Routes = [
  {path: 'star-wars-table', component: StarWarsTableComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
